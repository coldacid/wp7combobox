﻿// (c) Copyright Christopher S. Charabaruk and contributors.
// This source is subject to the Microsoft Public License (Ms-PL).

using System.Windows.Controls;
using Microsoft.Phone.Controls;
using System;
using System.Windows;
using System.Windows.Navigation;
using System.Windows.Controls.Primitives;

namespace Charabaruk.Controls.ComboPicker
{
    /// <summary>
    /// Class that implements a user-editable list-picking experience.
    /// </summary>
    public class ComboPicker : ItemsControl
    {
        private PhoneApplicationFrame _frame;
        private ComboPickerPage _pickerPage;
        private object _frameContentWhenOpened;

        private bool _hasPickerPageOpen;

        #region PickerPageUri

        public Uri PickerPageUri
        {
            get { return (Uri)GetValue(PickerPageUriProperty); }
            set { SetValue(PickerPageUriProperty, value); }
        }

        // Using a DependencyProperty as the backing store for PickerPageUri.  This enables animation, styling, binding, etc...
        public static readonly DependencyProperty PickerPageUriProperty =
            DependencyProperty.Register("PickerPageUri", typeof(Uri), typeof(ComboPicker), null);

        #endregion

        private void OpenPickerPage()
        {
            if (null == PickerPageUri)
            {
                throw new ArgumentNullException("PickerPageUri");
            }

            if (null == _frame)
            {
                _frame = Application.Current.RootVisual as PhoneApplicationFrame;
                if (null != _frame)
                {
                    _frameContentWhenOpened = _frame.Content;

                    _frame.Navigated += OnFrameNavigated;
                    _hasPickerPageOpen = true;
                    _frame.Navigate(PickerPageUri);
                }
            }
        }

        private void ClosePickerPage()
        {
            if (null != _frame)
            {
                _frame.Navigated -= OnFrameNavigated;
            }
        }

        private void OnFrameNavigated(object sender, NavigationEventArgs e)
        {
            if (e.Content == _frameContentWhenOpened)
            {
                // do nothing, we're returning from picker page
                return;
            }
            else if (null == _pickerPage && _hasPickerPageOpen)
            {
                _hasPickerPageOpen = false;
                _pickerPage = e.Content as ComboPickerPage;
                if (null != _pickerPage)
                {
                    // here's where we set stuff for the page

                    _pickerPage.Items.Clear();
                    if (null != Items)
                    {
                        foreach (var element in Items)
                        {
                            _pickerPage.Items.Add(element);
                        }


                    }

                }
            }
        }
    }
}
