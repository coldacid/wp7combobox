﻿// (c) Copyright Christopher S. Charabaruk and contributors.
// This source is subject to the Microsoft Public License (Ms-PL).

using System.Collections;
using System.Collections.Generic;
using Microsoft.Phone.Controls;

namespace Charabaruk.Controls.ComboPicker
{
    public partial class ComboPickerPage : PhoneApplicationPage
    {
        /// <summary>
        /// Gets or sets the list of items to select.
        /// </summary>
        public IList Items { get; private set; }

        /// <summary>
        /// Creates a list picker page.
        /// </summary>
        public ComboPickerPage()
        {
            InitializeComponent();

            Items = new List<object>();
        }
    }
}